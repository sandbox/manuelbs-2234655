<?php

/**
 * @file
 * Contains Entity API hooks and callbacks.
 */

/**
 * Implements hook_entity_property_info_alter().
 */
function base64property_entity_property_info_alter(&$info) {
  $file_properties = &$info['file']['properties'];
 
  $file_properties['base64'] = array(
    'label' => t('Base64'),
    'description' => 'Provides base64 representation of a file',
    'type' => 'text',
    'getter callback' => 'base64property_entity_get_properties',
  );
  
}

/**
 * Entity API property 'base64' getter callback.
 *
 * Support for %file:base64 tokens.
 */
function base64property_entity_get_properties($data, array $options, $name, $type, $info) {

  if ($type == 'file' && $name == 'base64') {
    //return file base64 encoded
    $file_path = drupal_realpath($data->uri); 
    $content = file_get_contents($file_path);

    return $content;
  }
}